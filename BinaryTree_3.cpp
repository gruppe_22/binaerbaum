#include "BinaryTree.h"

#include <stdio.h>
#include <iostream>


using namespace std;

/*
* Implementierung der Methoden searchKey() und insertKey()
* aus der Headerdatei "BinaryTree.h"
*
* @author Felix Engelhardt
*
* @date 05.11.2021 15:37
*
* @version 1.2
*/

BinaryTree::BinaryTree() {
    this->root = nullptr;
}

/*
* Sucht nach einem bestimmten Knoten, indem es den den Bin�rbaum durchl�uft
* und die Werte des aktuellen Knotens mit dem gesuchten vergleicht.
*
* @param Schl�ssel des gesuchten Knotens
*
* @return Struktur, die angibt, ob die Suche erfolgreich war und die Anzahl
* der Vergleiche sowie einen Zeiger auf den gesuchten Knoten zur�ckliefert.
*
* @since version 1.0
*/
Result BinaryTree::searchKey(const int key) {

    Node *current {this->root};

    Result result = {.node = this->root, .success = false, .comparisons = 0};

    if (!current)
        return result;


    while (current) {
        if (current->key == key) {
            return result = {.node = current, .success = true};
        } else if (key < current->key) {
            current = current->left;
            result.comparisons++;
        } else if (key > current->key) {
            current = current->right;
            result.comparisons++;
        }
    }

    return result;
}

/*
* F�gt einen neuen Knoten zu einem gegebenen Schl�ssel in den Bin�rbaum ein.
* Vorher wird noch �berpr�ft, ob der Knoten bereits vorhanden ist.
* Der Baum wird solange durchlaufen, bis der aktuelle Knoten ein Nullpointer ist.
* Bevor der Schl�ssel eingef�gt wird, muss noch der neue Knoten erstellt werden.
*
* @param neuer Schl�ssel
*
* @return Struktur vom Typ Result (s.o.)
*
* @since version 1.0
*/
Result BinaryTree::insertKey(const int key) {

    try {
        Result result = searchKey(key);

        Node *current {this->root};
        Node *predecessor {nullptr};

        if (result.success)
            return result = {.node = current, .success = false, .comparisons = 0};

        if (!current) {
            this->root = new Node;
            current = this->root;  // wichtig!
            current->prev = nullptr;
            current->key = key;
            current->left = nullptr;
            current->right = nullptr;
            return result = {.node = current, .success = true};
        }

        while (true) {   //current != nullptr || predecessor != nullptr
            if (!current) {
                current = new Node;
                current->key = key;
                current->prev = predecessor;
                if (current->key < predecessor->key)
                    current->prev->left = current;
                else
                    current->prev->right = current;
                current->left = nullptr;
                current->right = nullptr;
                return result = {.node = current, .success = true};
            } else if (key < current->key) {
                predecessor = current;
                current = current->left;
                result.comparisons++;
            } else if (key > current->key) {
                predecessor = current;
                current = current->right;
                result.comparisons++;
            }
        }

    } catch(const bad_alloc &e) {
        cerr << "Speicher konnte nicht beschaffen werden!";
    }

}
/*
*L�scht einen Knoten zu einem gegebenen Schl�ssel k
*
*@Schl�ssel des Knotens der gel�scht werden soll
*
*@return Struktur vom Typ Result (s.o.)
*
*@since version 1.2
*/
Result BinaryTree::removeKey(const int key) {

    Result result = searchKey(key);

    Node *current {result.node};
    Node *predecessor {current->prev};
    Node *current_new {current->right};

    bool lambdafunktion = [](Node* knoten) {};

    //Knoten hat keine Kinder ?
    if (!current->right && !current->left) {
        if (!predecessor) { //Knoten ist Root ?
            this->root = nullptr;
        } else {       //Zeiger des Vorg�ngers auf den Nachfolger muss auf nullptr gesetzt werden
            if (predecessor->right == current)
                predecessor->right = nullptr;
            else
                predecessor->left = nullptr;
        }
        delete current;
        return result = {.node=nullptr,.success=true};
    }

    //Knoten hat genau ein Kind => vier fallunterscheidungen
    if (!!current->right ^ !!current->left) {  //exklusives ODER
        if (!predecessor) {     //Knoten ist Root ?
                if (current->right)
                    this->root = current->right;
                else
                    this->root = current->left;
        } else if (predecessor->right == current && current->right) {  //knoten ist rechts und hat einen rechten nachfolger
                predecessor->right = current->right;
        } else if (predecessor->left == current && current->right) { //knoten ist links und hat einen rechten nachfolger
                predecessor->left = current->right;
        } else if (predecessor->right == current && current->left) {  //knoten ist rechts und hat einen linken nachfolger
                predecessor->right = current->left;
        } else if (predecessor->left == current && current->left) { //knoten ist links und hat einen linken nachfolger
                predecessor->left = current->left;
        }
        delete current;
        return result = {.node=nullptr,.success=true};
    }

    //Knoten hat zwei Kinder
    if (current->right && current->left) {
        current_new = current->right;
        while (current_new->left) {       //es wird nach dem Minimum vom rechten Teilbaum von current gesucht
            current_new = current_new->left;
        }
        current_new->right = current->right;  //das Minimum ersetzt dan den gel�schten Knoten
        current_new->left = current->left;
        if (!predecessor) {             //Knoten ist root ?
            this->root = current_new;
        } else {
            if (predecessor->right == current)
                predecessor->right = current_new;
            else
                predecessor->left = current_new;
        }
        delete current;
        return result = {.node=nullptr,.success=true};
    }

}
