#ifndef BINARYTREE_H_INCLUDED
#define BINARYTREE_H_INCLUDED

typedef struct Node {
    int key;
    Node *prev;
    Node *left;
    Node *right;
}Node;

typedef struct Result {
    Node *node;
    bool success;
    int comparisons;
}Result;

class BinaryTree {
private:
    Node *root;
public:
    BinaryTree();
    ~BinaryTree();
    Result insertKey(int key);
    Result searchKey(int key);
    Result removeKey(int key);
};



#endif // BINARYTREE_H_INCLUDED
